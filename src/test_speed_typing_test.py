import unittest
from unittest.mock import patch
import speed_typing_test
import os

os.environ["PYTEST_DISABLE_PLUGIN_AUTOLOAD"] = "1"


class SpeedTypingTestTestCase(unittest.TestCase):
    @patch('speed_typing_test.open', create=True)
    def test_load_text(self, mock_open):
        # Mock the file contents
        mock_open.return_value.__enter__.return_value.readlines.return_value = ['This is a test\n', 'Another line\n']
        # This tests loading text
        text = speed_typing_test.load_text()
        self.assertIn(text, ['This is a test', 'Another line'])
        mock_open.assert_called_once_with("text.txt", "r")


    @patch('speed_typing_test.time')
    def test_wpm_calculation(self, mock_time):
        # Mock the time elapsed and the length of current_text
        mock_time.time.side_effect = [100, 200]  # 100 seconds, 200 seconds
        current_text = ['T', 'h', 'i', 's', ' ', 'i', 's', ' ', 'a', ' ', 't', 'e', 's', 't']
       # This tests WPM calculation
        wpm = speed_typing_test.calculate_wpm(current_text)
        self.assertEqual(wpm, 4)  # (14 / (100 / 60)) / 5 = 4 WPM

    def test_load_text_empty_file(self):
        # Mock an empty file
        with patch('speed_typing_test.open', create=True) as mock_open:
            mock_open.return_value.__enter__.return_value.readlines.return_value = []
            # Test loading text from an empty file
            text = speed_typing_test.load_text()
            self.assertEqual(text, '')
            mock_open.assert_called_once_with("text.txt", "r")


    def test_wpm_calculation_empty_current_text(self):
        # Test WPM calculation with empty current_text
        wpm = speed_typing_test.calculate_wpm([])
        self.assertEqual(wpm, 0)

if __name__ == '__main__':
    unittest.main()
